﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public delegate void OnChangeDownloadingProgress(float progress);

public class AssetBundleManager : MonoBehaviour
{
    // url of the asset bundle
    [SerializeField]
    string loadUrl;

    // event that is called when downloading progress is changed
    public static event OnChangeDownloadingProgress onChangeProgress;

    // link to the downloaded bundle
    static AssetBundle bundle;

    // it used to know when to call onChangeProgress event
    float prevProgress;

    // link to the www
    WWW www;

    private void Start()
    {
        // Clear Cache
        Caching.CleanCache();

        // start downloading
        StartCoroutine(load(loadUrl, 1));
    }

    private void Update()
    {
        if (www != null)
        {
            // if current progress is not equal prevProgress
            if (www.progress != prevProgress)
            {
                // call on change progress event
                onChangeProgress(www.progress);

                // update prevProgress
                prevProgress = www.progress;
            }
        }
    }

    private IEnumerator load(string url, int version)
    {
        // call an event at first
        if (onChangeProgress != null)
        {
            onChangeProgress(0f);
        }

        Debug.Log("Start load method");

        // wait for the caching system to be ready
        while (!Caching.ready)
            yield return null;

        Debug.Log("Caching is ready");

        // load AssetBundle file from Cache if it exists with the same version or download and store it in the cache
        www = WWW.LoadFromCacheOrDownload(url, version);

        yield return www;

        if (!string.IsNullOrEmpty(www.error))
            throw new Exception("WWW download had an error: " + www.error);
        else
            Debug.Log("Downloading is complete");

        // assign asset bundle
        bundle = www.assetBundle;

        www = null;

        onDownloadingComplete();
    }

    private void onDownloadingComplete()
    {
        // load game scene when downloading content is complete
        SceneManager.LoadScene("Game");
    }

    public static T LoadAsset<T>(string assetName) where T : UnityEngine.Object
    {
        if (bundle != null)
        {
            // try to load asset from the bundle by assetName
            return bundle.LoadAsset<T>(assetName);
        }

        return null;
    }
}