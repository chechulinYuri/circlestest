﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Component that updates text with downloading progress of the asset bundle
/// </summary>
[RequireComponent(typeof(Text))]
public class DownloadingProgressUI : MonoBehaviour
{
    Text progressField;

    private void Awake()
    {
        // assign text field
        progressField = GetComponent<Text>();

        // sunbscribe to the onChange event
        AssetBundleManager.onChangeProgress += onChangeProgress;
    }

    private void OnDestroy()
    {
        // unsubscribe from the event
        AssetBundleManager.onChangeProgress -= onChangeProgress;
    }

    private void onChangeProgress(float progress)
    {
       if (progressField != null)
        {
            // translate normalized progress to percent
            var percent = (int)(progress * 100);

            // update text
            progressField.text = string.Format("Downloading content ({0}%)", percent);
        }
    }
}
