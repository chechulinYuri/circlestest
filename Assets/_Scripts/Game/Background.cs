﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Background : MonoBehaviour
{
    private void Start()
    {
        var sprite = AssetBundleManager.LoadAsset<Sprite>("Background");
        var image = GetComponent<Image>();

        image.sprite = sprite;
    }
}
