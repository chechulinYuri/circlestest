﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnCirclePop();

[RequireComponent(typeof(SpriteRenderer))]
public class CircleController : MonoBehaviour
{
    public event OnCirclePop OnPop;

    // min scale of the circle
    [SerializeField]
    float minSize = 1f;

    // max scale of the circle
    [SerializeField]
    float maxSize = 2.5f;

    [SerializeField]
    float baseSpeed = 0.25f;

    // shows how speed depends on size
    [SerializeField]
    AnimationCurve curveSpeedFromSize;

    // points = size * pointsFactor
    [SerializeField]
    float pointsFactor = 100f;

    // circle color range
    [SerializeField]
    Gradient colorRange;

    [SerializeField]
    ParticleSystem deathParticles;

    [SerializeField]
    AudioSource deathSound;

    // vertical move speed of the circle
    float speed;

    // how many points are given for the circle
    int points;

    // level of the circle
    int level;

    //
    float circlePhysicalSize;

    SpriteRenderer spriteRenderer;

    CircleCollider2D circleCollider;

    // shows if gameobject is active
    public bool isActive { get { return gameObject.activeInHierarchy; } }

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        circleCollider = GetComponent<CircleCollider2D>();
    }

    public void reset()
    {
        // enable collider
        circleCollider.enabled = true;

        // enable sprite renderer
        spriteRenderer.enabled = true;

        // enable circle
        gameObject.SetActive(true);

        // randomize size
        var size = Random.Range(minSize, maxSize);

        // set local scale
        transform.localScale = new Vector3(size, size, size);

        // generate speed and points values
        var normalizedSize = (size - minSize) / (maxSize - minSize);

        var factorFromCurve = curveSpeedFromSize.Evaluate(normalizedSize);

        var speedWithoutLevel = factorFromCurve + baseSpeed;

        // add level factor to the speed
        speed = speedWithoutLevel + speedWithoutLevel * (level / 5f);

        points = (int)((1f - normalizedSize) * pointsFactor);

        // generate random color from the range
        var randomValue = Random.Range(0f, 1f);
        spriteRenderer.color = colorRange.Evaluate(randomValue);

        // update position
        setStartPisition();
    }

    public void setLevel(int level)
    {
        this.level = level;
    }

    private void setStartPisition()
    {
        var collider = GetComponent<CircleCollider2D>();

        // calc physical circle size
        circlePhysicalSize = collider.radius * 2f * transform.localScale.x;

        // calc spawn y
        var positionY = Camera.main.orthographicSize + circlePhysicalSize / 2f;

        // calc half of screen width
        var physicalScreenWidth = Camera.main.orthographicSize * (Screen.width / (float)Screen.height);

        // calc min and max spawn x
        var minSpawnX = -physicalScreenWidth + circlePhysicalSize / 2f;
        var maxSpawnX = physicalScreenWidth - circlePhysicalSize / 2f;

        // randomize x position between min and max
        var positionX = Random.Range(minSpawnX, maxSpawnX);

        transform.position = new Vector2(positionX, positionY);
    }

    private void FixedUpdate()
    {
        // if object is out of view
        if (transform.position.y < -Camera.main.orthographicSize - circlePhysicalSize)
        {
            disable();
        }
        else
        {
            // move object down
            transform.Translate(Vector2.down * speed);
        }
    }

    private void OnMouseDown()
    {
        // add points
        ScoreController.AddPoints(points);

        StartCoroutine(pop());
    }

    public void disable()
    {
        gameObject.SetActive(false);
    }

    IEnumerator pop()
    {
        // hide circle sprite
        spriteRenderer.enabled = false;

        // disable collider
        circleCollider.enabled = false;

        // emmit particles
        deathParticles.Emit(10);

        // play pop sound
        deathSound.Play();

        // call an event
        if (OnPop != null)
            OnPop();

        // wait
        yield return new WaitForSeconds(2f);

        // disable whole circle object
        disable();
    }
}