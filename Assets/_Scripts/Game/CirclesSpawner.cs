﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnLevelUpdated(int level);

public class CirclesSpawner : MonoBehaviour
{
    public static event OnLevelUpdated onLevelUpdated;

    [SerializeField]
    int maxCirclesNumber = 15;

    int popedCirclesCounter;
    int currentLevel;

    CircleController[] spawnedCircles;

    GameObject circlePrefab;

    // returns a number of active circles
    int activeCircles
    {
        get
        {
            if (spawnedCircles != null)
            {
                int active = 0;

                for (int i = 0; i < spawnedCircles.Length; i++)
                {
                    if (spawnedCircles[i].isActive)
                    {
                        active++;
                    }
                }

                return active;
            }

            return 0;
        }
    }

    private IEnumerator Start()
    {
        yield return initialize();

        yield return spawn();
    }

    IEnumerator spawn()
    {
        while (true)
        {
            var maxCirclesNumber = Mathf.Clamp(currentLevel * 2, 1, spawnedCircles.Length);

            if (activeCircles < maxCirclesNumber)
            {
                // activate circle
                var circle = activateRandomCircle();

                if (circle != null)
                {
                    // set circle level
                    circle.setLevel(currentLevel);
                }
            }

            yield return new WaitForSeconds(0.75f);
        }
    }

    CircleController activateRandomCircle()
    {
        for (int i = 0; i < spawnedCircles.Length; i++)
        {
            if (!spawnedCircles[i].isActive)
            {
                spawnedCircles[i].reset();

                return spawnedCircles[i];
            }
        }

        return null;
    }

    void onCirclePop()
    {
        // increment circles counter
        popedCirclesCounter++;

        // update level if needed
        if (popedCirclesCounter % 5 == 0)
        {
            currentLevel++;

            if (onLevelUpdated != null)
                onLevelUpdated(currentLevel);
        }
    }

    IEnumerator initialize()
    {
        currentLevel = 0;
        popedCirclesCounter = 0;
        spawnedCircles = new CircleController[maxCirclesNumber];
        circlePrefab = AssetBundleManager.LoadAsset<GameObject>("CircleObject");

        for (int i = 0; i < maxCirclesNumber; i++)
        {
            // instantiate circle
            var circleObject = Instantiate(circlePrefab, Vector3.zero, Quaternion.identity, transform) as GameObject;

            // get circleController component
            var circleComponent = circleObject.GetComponent<CircleController>();

            // assign circle component
            spawnedCircles[i] = circleComponent;

            // disable on start
            circleComponent.disable();

            // subscribe to on pop event
            circleComponent.OnPop += onCirclePop;

            yield return null;
        }
    }
}
