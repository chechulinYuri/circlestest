﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUpPanel : MonoBehaviour
{
    [SerializeField]
    Text levelUpMessage;

    [SerializeField]
    Animator levelUpAnimator;

    private void Awake()
    {
        CirclesSpawner.onLevelUpdated += onLevelUp;
    }

    private void OnDestroy()
    {
        CirclesSpawner.onLevelUpdated -= onLevelUp;
    }

    private void onLevelUp(int level)
    {
        levelUpMessage.text = string.Format("<b>{0}</b>", level + 1);

        levelUpAnimator.SetTrigger("show");
    }
}
