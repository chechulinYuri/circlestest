﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    static ScoreController instance;

    [SerializeField]
    Text scoreField;

    int totalScore;

    private void Awake()
    {
        instance = this;

        updateScoreText();
    }

    public static void AddPoints(int points)
    {
        if (instance != null)
        {
            instance.addPoints(points);
        }
    }

    void addPoints(int points)
    {
        totalScore += points;

        updateScoreText();
    }

    void updateScoreText()
    {
        scoreField.text = string.Format("Score: <b>{0}</b>", totalScore);
    }
}
