﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Component that loads and plays main theme audio
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class MainThemeAudio : MonoBehaviour
{
    static MainThemeAudio instance;

    // stores in player prefs if we should play main theme
    static bool isEnabled
    {
        get
        {
            return PlayerPrefs.GetInt("MainThemeAudioEnabled", 1) == 1;
        }
        set
        {
            PlayerPrefs.SetInt("MainThemeAudioEnabled", value ? 1 : 0);
        }
    }

    AudioSource source;

    private void Awake()
    {
        instance = this;

        CirclesSpawner.onLevelUpdated += onLevelUpdated;
    }

    private void onLevelUpdated(int level)
    {
        // update sound pitch to make the main theme fater when level is updated
        source.pitch = Mathf.Clamp(1f + level / 20f, 1f, 2f);
    }

    private void Start()
    {
        initialize();
    }

    private void initialize()
    {
        // assign audio source
        source = GetComponent<AudioSource>();

        // make it looped
        source.loop = true;

        // load clip from the bundle
        var clip = AssetBundleManager.LoadAsset<AudioClip>("Main theme");

        // assign audio clip
        source.clip = clip;

        if (isEnabled)
            // start playing the audio
            Play();
    }

    // play main theme audio
    public static void Play()
    {
        if (instance != null)
        {
            if (instance.source != null)
            {
                instance.source.Play();
            }
        }

        // save to player prefs
        isEnabled = true;
    }

    // stop main theme audio
    public static void Stop()
    {
        if (instance != null)
        {
            if (instance.source != null)
            {
                instance.source.Stop();
            }
        }

        // save to player prefs
        isEnabled = false;
    }
}
